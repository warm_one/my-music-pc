const CracoLessPlugin = require('craco-less');
// const CracoAntdPlugin = require('craco-antd');

const path = require('path');
module.exports = {
    plugins: [{
        plugin: CracoLessPlugin,
        options: {
            lessLoaderOptions: {
                lessOptions: {

                    javascriptEnabled: true,
                },
            },
        },
    }, ],
    babel: {
        plugins: [
            [
                "import",
                {
                    "libraryName": "antd",
                    "libraryDirectory": "es",
                    "style": true //设置为true即是less
                }
            ]
        ]
    },
    webpack: {
        // 别名
        alias: {
            "@": path.resolve("src"),
            "components": path.resolve("src/components"),
            'pages': path.resolve("src/pages")
        }
    },
};

// 配置别名
//在craco.config,.js里加上

// module.exports = {
//   webpack: {
//     // 别名
//     alias: {
//       "@": path.resolve("src"),
//       "@utils": path.resolve("src/utils"),
//     }
//   },
// }
// react脚手架 + antd的craco.config.js配置
// https://www.jianshu.com/p/bbdf2a50903a/

// {
//     plugin: CracoLessPlugin,
//     options: {
//         lessLoaderOptions: {
//             lessOptions: {

//                 javascriptEnabled: true,
//             },
//         },
//     },
// },
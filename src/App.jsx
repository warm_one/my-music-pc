import React, { } from 'react'
// import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Home from './pages/home'
// import store from './store'
function App() {
    return (<BrowserRouter  >
        <Home />
    </BrowserRouter>
    );
}

export default App;
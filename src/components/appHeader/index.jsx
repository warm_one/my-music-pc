import React, { memo } from "react";

import { NavLink } from "react-router-dom";
import { navLinkData } from "../../constans";

import './index.less';
const AppHeader = () => {
  return <div className='wHeader'>
    <div className='wrap flex1'>
      <h1 className='logo sprite_icon1'>
        <a href="/">
          网易云音乐
        </a>
      </h1>
      <ul className='flex1'>
        {
          navLinkData.map(v => <li className='navItem' key={v.path}  >
            <NavLink to={v.path} activeClassName='linkAct' className='link' target={v.target ? '_blank' : '_self'} >{v.title}
              <i className='dot sprite_icon1'></i>
            </NavLink>
          </li>)
        }
      </ul>
    </div>
  </div>
}
export default memo(AppHeader)
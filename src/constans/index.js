export const navLinkData = [{
        title: '发现音乐',
        path: '/'
    },
    {
        title: '我的音乐',
        path: '/my'
    },
    {
        title: '朋友',
        path: '/friend'
    },
    {
        title: '商店',
        path: 'https://music.163.com/store/product',
        target: true
    },
    {
        title: '音乐人',
        path: 'https://music.163.com/st/musician',
        target: true
    },
    {
        title: '下载客户端',
        path: '/download'
    },
]
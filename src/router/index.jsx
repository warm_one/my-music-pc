
import React from 'react';
import Home from '../pages/home';
import Discover from '../pages/discover'
const routes = [
  {
    path: '/',
    component: <Home />,
    children: [
      {
        path: '/',
        exact: true,
        redirect: '/discover'
      },
      {
        path: '/discover',
        component: <Discover />,
        children: [
        ]
      }
    ]
  }


]
export default routes